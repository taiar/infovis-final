
var map = new google.maps.Map(d3.select("#map").node(), {
  zoom: 18,
  center: new google.maps.LatLng(-19.924134, -43.924284),
  mapTypeId: google.maps.MapTypeId.TERRAIN
});

var ray = 20;

/* load data */
d3.json("cruzamentos.json", function(data) {
  var overlay = new google.maps.OverlayView();

  /* add overlay to gmaps */
  overlay.onAdd = function() {
    var layer = d3.select(this.getPanes().overlayLayer)
                  .append("div")
                  .attr("class", "crossing");

    /* draw overlays */
    overlay.draw = function() {
      var projection = this.getProjection();

      var marker = layer.selectAll("svg")
                        .data(d3.entries(data))
                        .each(transform) // update existing markers
                        .enter().append("svg:svg")
                        .each(transform)
                        .attr("class", "marker")
                        .style("opacity", 0.8)
                        .style("fill", function(d) {
                          var h = Math.floor(80 - d.value.score * 100);
                          var s = 0.7; 
                          var l = 0.5;
                          var color = d3.hsl(h, s, l);
                          return color;
                        });

      // Add a circle.
      marker.append("svg:circle")
            .attr("r", ray)
            .attr("r", function(d) { return ray * d.value.score; })
            .attr("cx", function(d) { return ray * d.value.score; })
            .attr("cy", function(d) { return ray * d.value.score; })
            .attr("title", function(d) { return d.value.name; });

      /* label */
      /*
      marker.append("svg:text")
            .attr("x", 33)
            .attr("dy", 28)
            .text(function(d) { return d.value.name; });
      */

      function transform(d) {
        d = new google.maps.LatLng(d.value.lng - 0.0005, d.value.lat - 0.0005);
        d = projection.fromLatLngToDivPixel(d);
        return d3.select(this)
                 .style("left", (d.x - ray) + "px")
                 .style("top", (d.y - ray) + "px");
      }
    };
  };

  /* bind the overlay over the map */
  overlay.setMap(map);
});
