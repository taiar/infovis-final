# coding: utf-8
#!/usr/bin/env python

import json
from pymongo import Connection
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import QueryParser, OrGroup

# get tweets from mongo
print 'Connecting to mongodb'
con = Connection('staff.mongohq.com', 10080)

db  = con['infovis_tweets']
db.authenticate('taiar', 'taiar')

tweets = db['tweets']

# index tweets
print 'Indexing tweets'
schema = Schema(text=TEXT(stored=True))
ix = create_in("tweets", schema)
writer = ix.writer()

for tweet in tweets.find():
    writer.add_document(text=unicode(tweet['text']))
writer.commit()

# open the crossings csv
crossings = []
print 'Getting crossings'
grafo = open('grafo.csv', 'r')
    
# try to match any tweet
print 'Matching tweets'
max_score = 0
min_score = 999999999
parser = QueryParser('text', schema=ix.schema, 
                     group=OrGroup)
with ix.searcher() as searcher:
    for cross in grafo.readlines()[1:]:
        spl = unicode(cross.strip()).split(';')
        name = spl[0][1:-1] + u" com " + spl[1][1:-1]

        myquery = parser.parse(name)
        results = searcher.search(myquery)
        if len(results) > 3:
            score = 0
            for r in results:
                score += r.score
            # write crossing data
            if score > 130:
                print name, score
                crossings.append({ 'name': name,
                                   'lat': spl[2],
                                   'lng': spl[3],
                                   'score': score })

            min_score = score if score < min_score else min_score
            max_score = score if score > max_score else max_score

# export normalized json
print 'Normalizing scores'
print 'Min score:', min_score 
print 'Max score:', max_score 

for cross in crossings:
    cross['score'] = (cross['score'] - min_score) / max_score

# dumping json
print 'Dumping json'
with open('cruzamentos2.json', 'w') as out:
    out.write(json.dumps(crossings))
