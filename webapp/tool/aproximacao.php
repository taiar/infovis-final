<?php
// input misspelled word
$input = 'BERNARDO VASCONCELOS';

// array of words to check against
$words  = array('BERNARDO DE VASCONCELOS', 'ANTONIO PEREGRINO NASCIMENTO', 'ANTONIO PEREGRINO NASCIMENTO', 'AQUINO E CASTRO', 'BONFIM DE ABREU', 'CARMELITA FARIA GAROFALO', 'CLAUDIO GOMES DE SOUZA', 'CONCEICAO VIDIGAL PAULUCCI', 'CORONEL JAIRO PEREIRA', 'FRANCISCO LUIZ', 'FRANCISCO NUNES', 'JOAQUIM RIBEIRO COSTA', 'JORGE ANGEL LIVRAGA', 'JOSE LAKTIN', 'LUIZ PECANHA', 'MANOEL JOSE DA SILVA', 'MANOEL VENANCIO MARTINS', 'MARIO DE LIMA', 'MEIRA DE VASCONCELOS', 'PADRE FELIPE DA SILVA', 'PASTOR ACHILLES BARBOSA', 'PRADO PIMENTEL', 'PROFESSOR PEDRO ALVARENGA', 'SALVADOR GURGEL');

// no shortest distance found, yet
$shortest = -1;

// loop through words to find the closest
foreach ($words as $word) {

    // calculate the distance between the input word,
    // and the current word
    $lev = levenshtein($input, $word);

    // check for an exact match
    if ($lev == 0) {

        // closest word is this one (exact match)
        $closest = $word;
        $shortest = 0;

        // break out of the loop; we've found an exact match
        break;
    }

    // if this distance is less than the next found shortest
    // distance, OR if a next shortest word has not yet been found
    if ($lev <= $shortest || $shortest < 0) {
        // set the closest match, and shortest distance
        $closest  = $word;
        $shortest = $lev;
    }
}

echo "Input word: $input\n";
if ($shortest == 0) {
    echo "Exact match found: $closest\n";
} else {
    echo "Did you mean: $closest?\n";
}
