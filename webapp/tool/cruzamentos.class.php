<?php

class Cruzamento {
  
  private $r1 =  FALSE;
  private $r2 =  FALSE;
  private $lat = FALSE;
  private $lng = FALSE;
  
  private $db;

  public function __construct($r1 = '', $r2 = '') {
    if(!empty($r1))
      $this->addStreet($r1);
    if(!empty($r2))
      $this->addStreet($r2);
    $this->db = database::getInstance();
  }

  public function addStreet($name) {
    $name = $this->clearString($name);
    if(!$this->r1)
      $this->r1 = (empty($name))?FALSE:$name;
    else
      $this->r2 = (empty($name))?FALSE:$name;
  }

  public function setR1($name) {
    $this->r1 = $this->clearString($name);
  }

  public function setR2($name) {
    $this->r2 = $this->clearString($name);
  }

  public function clearStreets() {
    $this->r1 = FALSE;
    $this->r2 = FALSE;
  }

  public function showStreet() {
    echo "(" . $this->r1 . ";" . $this->r2 . ")\n";
  }

  public function showCoord() {
    echo "(" . $this->lat . ";" . $this->lng . ")\n";
  }

  private function clearString($name) {
    $array1 = array("á","à","â","ã","ä","é","è","ê","ë","í","ì",
                    "î","ï","ó","ò","ô","õ","ö","ú","ù","û","ü",
                    "ç","Á","À","Â","Ã","Ä","É","È","Ê","Ë","Í",
                    "Ì","Î","Ï","Ó","Ò","Ô","Õ","Ö","Ú","Ù","Û",
                    "Ü","Ç");
    $array2 = array("a","a","a","a","a","e","e","e","e","i","i",
                    "i","i","o","o","o","o","o","u","u","u","u",
                    "c","A","A","A","A","A","E","E","E","E","I",
                    "I","I","I","O","O","O","O","O","U","U","U",
                    "U","C");
    $name = str_replace($array1, $array2, $name);
    $name = trim($name);
    return strtoupper($name);
  }

  private function fixCoords() {
    $this->lat -= .0005;
    $this->lng -= .0005;
  }

  public function getCoords($redo = TRUE) {
    if(!$this->r1 || !$this->r2)
      return false;
    else {
      $res = $this->db->find("select * from cruzamentos where r1 like '%" . $this->r1 . "%' and r2 like '%" . $this->r2 . "%'");
      if($res['total'] > 1) {
        echo "Mais de um resultado para o cruzamento foi encontrado\n";
        return false;
      } 
      else if($res['total'] == 0) {
        echo "Nenhum resultado encontrado\n";
        if($redo)
          $this->getAprox();
        return false;
      }
      else {
        $this->lat = $res['dados'][0]['lat'];
        $this->lng = $res['dados'][0]['lng'];
        $this->fixCoords();
        return true;
      }
    }
  }

  public function getAprox() {
    $lista = array();
    $res = $this->db->find("select * from cruzamentos where r1 like '%" . $this->r1 . "%'");
    if($res['total'] > 0) {
      foreach ($res['dados'] as $chave)
        $lista[] = trim(str_replace("AVENIDA", "", str_replace("RUA", "", $chave['r2'])));
      $this->setR1($this->r1);
      $this->setR2($this->didYouMean($this->r2, $lista));
    }
    else {
      $res = $this->db->find("select * from cruzamentos where r1 like '%" . $this->r2 . "%'");
      if($res['total'] > 0) {
        foreach ($res['dados'] as $chave)
          $lista[] = trim(str_replace("AVENIDA", "", str_replace("RUA", "", $chave['r2'])));
        $this->setR2($this->r2);
        $this->setR1($this->didYouMean($this->r1, $lista));
      } else
        return false;
    }
    $this->getCoords(FALSE);
  }

  public function didYouMean($palavra, $words) {
    $shortest = -1;
    $closest = "";
    foreach ($words as $word) {
      $lev = levenshtein($palavra, $word);
      if ($lev == 0) {
        $closest = $word;
        $shortest = 0;
        break;
      }
      if ($lev <= $shortest || $shortest < 0) {
        $closest  = $word;
        $shortest = $lev;
      }
    }
    if($closest > 10) return false;
    return $closest;
  }
}

require("lib/erro.class.php");
require("lib/database.class.php");

$c = new Cruzamento("afonso pena", "nossa senhora do carmo");

$c->getCoords();
$c->showStreet();
$c->showCoord();
