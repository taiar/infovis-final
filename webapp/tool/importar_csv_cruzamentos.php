<?php

require("lib/erro.class.php");
require("lib/database.class.php");
require("lib/util.class.php");

$db = database::getInstance();

$src = file('../../gis/grafo.csv');
$c = count($src);

for ($i=1; $i < $c; $i++) { 
  $fields = explode(";", $src[$i]);
  $db->insert('cruzamentos', array(
    'r1' => trim(str_replace("\"", "", $fields[0])),
    'r2' => trim(str_replace("\"", "", $fields[1])),
    'lat' => trim($fields[3]),
    'lng' => trim($fields[2])
  ));
}
