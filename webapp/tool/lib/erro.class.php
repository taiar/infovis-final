<?php

class erro {

  var $append_message;

  function __construct() {
    if(!isset($_SESSION["error"]))
      $_SESSION["error"] = array();

    if(!isset($_SESSION["message"]))
      $_SESSION["message"] = array();

    $this->append_message = "On " . date('H:i:s - d/m/Y') . " | in " . $_SERVER["SCRIPT_FILENAME"] . " | ";
  }

  function add_error($err) {
    $_SESSION["error"][] = $this->append_message . $err;
  }

  function add_message($msg) {
    $_SESSION["message"][] = $this->append_message . $msg;
  }

  function clean_all() {
    unset($_SESSION["message"]);
    unset($_SESSION["error"]);
  }

  function show_all() {
    UTIL::veArray($this->errors);
    UTIL::veArray($this->messages);
  }
		
  public function cleanMessages() {
    unset($_SESSION["message"]);
  }

  public function cleanError() {
    unset($_SESSION["error"]);
  }
}

?>