<?php

class util {

	public function cleanNames($str) {
		$str = addslashes(trim(strip_tags($str)));
		return $str;
	}

	public static function __unserialize($sObject) {
		$__ret = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $sObject );
		return unserialize($__ret);
	}

	public static function veArray($arr) {
		echo "<pre>";
		echo print_r($arr);
		echo "</pre>";
	}

	public static function redir($targ) {
		header("location:" . $targ);
    exit;
	}

	public static function c($str) {
		$str = base64_encode($str);
		return $str;
	}

	public static function dc($str) {
		$str = base64_decode($str);
		return $str;
	}

	public function upload($file, $loc, $size = '', $ext = '') {
		//checka se o tamanho é adequado
		if ($size != "")
			if ($file['size'] > $size)
				return false;

		//checka se a extensao do arquivo é adequada
		if (is_array($ext)) {
			$ver = 0;
			for ($i = 0; $i < count($ext); $i++)
				if (!ereg(".".$ext[0]."$", $file['name']))
					$ver++;
			if ($ver > 0)
				return false;
		}

		//checka se o arquivo realmente existe...
		if (! empty($file['name']) and is_file($file['tmp_name']))
			if (copy($file['tmp_name'], $loc))
				return true;
			else
				return false;
		else
			return false;
	}

	public static function renameFile($name, $pos='') {
        $tempo = time();
        $arr = explode('.', $name);
        $ext = end($arr);
        $new = md5($name . $pos . $tempo);
        $new = $new . "." . $ext;
        return $new;
	}

	public static function getOsSlash() {
	  return (PHP_OS == "Linux")?"/":"\\";
	}

	public function mudaNome($nome) {
		$new = strtolower($nome);
		$new = str_replace(" ", "", $new);
		$new = str_replace("á", "a", $new);
		return $new;
	}

	public function calculaTamanhoKbytes($bytes) {
		$kbytes = $bytes / 1024;
		$kbytes = number_format($kbytes,2);
		return $kbytes . " kb";
	}

	public static function isInArray($valor, $array) {
		if(!current($array))
			return false;
		foreach ($array as $key => $value) {
			if($value == $valor)
				return $key;
		}
	}

}
