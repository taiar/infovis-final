<?php

/**
 * FIXME Esta classe nao deve ter instancia de banco de dados uma vez que os dados do usuário
 * devem ser encontrados atraves da classe de usuário.
 */

class auth {

  private $error;

  private $login;
  private $senha;
  private $sessFlags;
  public  $logFlag;
  private $db;
  private $data;

  private $user;

  public function __construct($login = '', $senha = '') {
  	$this->error = new erro();
  	
    $this->login = $login;
    $this->senha = $senha;

    $this->user = new User();

    if (!$this->testSession()) {
      $this->sessFlags = false;
      $this->error->add_message("Sessions are not initialized.");
    } else
      $this->sessFlags = true;
    $this->setLogFlag();
  }

  /**
   * Wrapper para verificação de logins
   */
  public function logadoOuRedireciona($url = '') {
    if(!$this->logFlag)
      if(empty($url))
        util::redir(PAGE_FORM_LOGIN);
      else 
        util::redir($url);
  }

  public function setLogFlag() {
    if ($this->sessFlags && isset($_SESSION[SESSID]))
      $this->logFlag = true;
    else
      $this->logFlag = false;
  }

  public function testSession() {
    if (session_id() == '')
      return false;
    return true;
  }

  public function validate() {
    if(!$this->user->getByLogin($this->login))
      return false;
    else if($this->user->senha != $this->senha)
      return false;
    else
      return true;
  }

  /**
   * FIXME: alterar a função para verificar se o usuário é um administrador
   */
  public function validateAdmin() {
  	$this->data = $this->db->find("select id, login, senha from " . DB_TABLE_USERS . " where login like '" . util::c($this->login) . "' limit 1");
    if($this->data['total'] < 1)
      return false;
    else if($this->user->senha != util::c($this->senha))
      return false;
    else
      return true;
  }

  public function doAuth() {
    // Verifica session_start
    if (!$this->sessFlags) {
      $this->error->add_error("Sessions where not initialized.");
      return false;
    }
    if ($this->validate()) {
      $_SESSION[SESSID] = $this->user->id;
      return true;
    }
    else {
      $this->undoAuth();
      $this->error->add_message("Invalid login");
      return false;
    }
  }

  public function undoAuth() {
    unset($_SESSION[SESSID]);
  }

  public function getUser() {
    if($this->logFlag)
      return $_SESSION[SESSID];
  }

  public function isAdmin() {
    if ($this->logFlag)
      return true;
    else {
      $this->error->add_error("User not logged.");
      return false;
    }
  }

  public function getSession() {
    return $_SESSION[SESSID];
  }

  public function doAdminValidation() {
  	if (!$this->sessFlags) {
      $this->error->add_error("Sessions where not initialized.");
      return false;
    }
    if ($this->validateAdmin()) {
      $_SESSION[SESSID] = $this->data['dados'][0]['id'];
      return true;
    } else {
      $this->undoAuth();
      $this->error->add_message("Invalid login");
      return false;
    }
  }
}
