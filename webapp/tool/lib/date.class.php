<?php

  class date
  {
    var $in;
    var $unix_time;
    var $date;
    var $time;
    var $db_date;
    var $type;

    var $month_names;

    var $unit_dia;
    var $unit_mes;
    var $unit_ano;

    var $dataErr;

    function __construct ($in = '')
    {
      $this->dataErr = "";

      if(empty($in))
        $this->in = time();
      else
        $this->in = $in;

      if(!$this->set_type())
      {
        $this->dataErr = "Formato de data inv�lido";
        return false;
      }

      if($this->type != "UNIXTIME")
        $this->to_unix_time($this->in);
      else
      {
        $this->unix_time = $this->in;
        $this->unit_ano = date("Y", $this->in);
        $this->unit_mes = date("m", $this->in);
        $this->unit_dia = date("d", $this->in);
      }

      $this->set_date();

      $this->set_db_date();
    }

    function set_type()
    {
      if(ereg("[0-9]{10,11}", $this->in))
      {
        $this->type = "UNIXTIME";
        return true;
      }
      else if(ereg("[0-9]{1,2}(-|/)[0-9]{1,2}(-|/)[0-9]{4}", $this->in))
      {
        $this->type = "DATE";
        return true;
      }
      else if(ereg("DATETIME", $this->in))
      {
        $this->type = "DATETIME";
        return true;
      }
      else if(ereg("[0-9]{4}(-|/)[0-9]{1,2}(-|/)[0-9]{1,2}", $this->in))
      {
        $this->type = "DBDATE";
        return true;
      }
      else if(ereg("DBDATETIME", $this->in))
      {
        $this->type = "DBDATETIME";
        return true;
      }
      else
        return false;
    }

    function to_unix_time()
    {
      $data = str_replace("/", "-", $this->in);
      $data = explode("-", $data);

      if($this->type == "DBDATE")
      {
        $ano = $data[0];
        $this->unit_ano = $ano;
        $mes = $data[1];
        $this->unit_mes = $mes;
        $dia = $data[2];
        $this->unit_dia = $dia;
      }

      else if($this->type = "DATE")
      {
        $ano = $data[2];
        $this->unit_ano = $ano;
        $mes = $data[1];
        $this->unit_mes = $mes;
        $dia = $data[0];
        $this->unit_dia = $dia;
      }

      else
      {
        echo "Not implemented...\n";
        exit;
      }

      $this->unix_time = mktime(0, 0, 0, $this->unit_mes, $this->unit_dia, $this->unit_ano);
    }

    function set_month_names()
    {
      $this->month_names = array();
      $this->month_names['01'] = "Janeiro";
      $this->month_names['02'] = "Fevereiro";
      $this->month_names['03'] = "Março";
      $this->month_names['04'] = "Abril";
      $this->month_names['05'] = "Maio";
      $this->month_names['06'] = "Junho";
      $this->month_names['07'] = "Julho";
      $this->month_names['08'] = "Agosto";
      $this->month_names['09'] = "Setembro";
      $this->month_names['10'] = "Outubro";
      $this->month_names['11'] = "Novembro";
      $this->month_names['12'] = "Dezembro";
      $this->month_names['1'] = "Janeiro";
      $this->month_names['2'] = "Fevereiro";
      $this->month_names['3'] = "Março";
      $this->month_names['4'] = "Abril";
      $this->month_names['5'] = "Maio";
      $this->month_names['6'] = "Junho";
      $this->month_names['7'] = "Julho";
      $this->month_names['8'] = "Agosto";
      $this->month_names['9'] = "Setembro";
    }

    function set_date()
    {
      $this->date = date("d-m-Y", $this->unix_time);
    }

    // function set_time() // proximas versoes
    // {}

    function set_db_date()
    {
      $this->db_date = date("Y-m-d", $this->unix_time);
    }

    function get_type()
    {
      return $this->type;
    }

    function get_date($sep = '')
    {
      if(empty($sep))
        return $this->date;
      else
        return str_replace("-", $sep, $this->date);
    }
		
		function get_date_MY($sep = '')
		{
			if(empty($sep))
			 $sep = "-";
			return $this->unit_mes . $sep . $this->unit_ano; 
		}

    function get_db_date($sep = '')
    {
      if(empty($sep))
        return $this->db_date;
      else
        return str_replace("-", $sep, $this->db_date);
    }

    function get_textual_date()
    {
      $this->set_month_names();
      $str = $this->unit_dia . " de " . $this->month_names[$this->unit_mes] . " de " . $this->unit_ano;
      return $str;
    }

    function gotErr()
    {
      if(!empty($this->dataErr))
        return true;
      return false;
    }
  } // end class

?>
