<?php

/**
 * TODO adaptar classe para trabalhar com PHP PDo
 */

class database {

  var $user	= "root";
  var $pass	= "taiar";
  var $db =   "infovis";
  var $host	= "localhost";

  private $error;
  private $init = false;

  private static $instance; // Singleton

  public static function getInstance() {
    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c;
    }
    return self::$instance;
  }

  private function init() {
    $this->error = new erro();
    if(!@mysql_connect($this->host, $this->user, $this->pass)) {
      $this->error->add_error("SQL error(1): " . mysql_error());
      return false;
    }
    if(!@mysql_select_db($this->db)) {
      $this->error->add_error("SQL error(2): " . mysql_error());
      return false;
    }
    mysql_set_charset('UTF8');
    $this->init = true;
  }

  private function __construct($select = true) {
    // nothing...
  }

  public function __clone() {
    trigger_error('Clone is not allowed.', E_USER_ERROR);
  }

  public function del($table, $cond) {

    if(!$this->init)
      $this->init();

    if(!@mysql_query("delete from " . $table . " where " . $cond . "")) {
      $this->error->add_error("SQL error(4): " . mysql_error());
      return false;
    }
    else
      return true;
  }

  public function update($table, $array, $key, $id) {

    if(!$this->init)
      $this->init();

    $erro = 0;

    foreach($array as $fild => $val) {
      if(!mysql_query("update " . $table . " set " . $fild . "='" . $val . "' where " . $key . "='" . $id . "'"))
        $erro++;
    }

    if($erro > 0) {
      $this->error->add_error("SQL error(5): " . mysql_error());
      return false;
    }
    else
      return true;
  }

  public function insert($table, $array) {

    if(!$this->init)
      $this->init();

    $campo = array();
    $valor = array();

    foreach($array as $fild => $val) {
      $campo[] = $fild;
      $valor[] = $val;
    }

    $valores = implode("', '", $valor);
    $campos = implode(", ", $campo);

    if(!@mysql_query("insert into " . $table . " (" . $campos . ") values ('" . $valores . "')")) {
      $this->error->add_error("SQL error(6): " . mysql_error());
      return false;
    }

    else {
      $id = @mysql_insert_id();
      return $id;
    }
  }

  public function tables() {

    if(!$this->init)
      $this->init();

    $arr = array();
    $query = @mysql_query("show tables from " . DB_NAME);
    while ($row = mysql_fetch_row($query))
     $arr['dados'][] = $row[0];
    $arr['total'] = mysql_num_rows($query);
    return $arr;
  }

  public function find($sql) {

    if(!$this->init)
      $this->init();

    $query = @mysql_query($sql);
    if(!$query) {
      $this->error->add_error("SQL error(7): " . mysql_error());
      return false;
    }
    else {
      $ret = array();
      $ret['total'] = @mysql_num_rows($query);
      while($l = @mysql_fetch_array($query)) {
        foreach($l as $key => $val)
        $l[$key] = $val;
        $ret['dados'][] = $l;
      }
      return $ret;
    }
  }
}
