require 'rubygems'
require 'open-uri'
require 'json'
require 'mongo'

twitters = ['BlitzBH', 'TransitoBH', 'OficialBHTRANS', 'BlitzGrandeBH', 'correndodeblitz', 'LeiSecaBH']
quantidade = 50
novos = 0

db = Mongo::Connection.new("staff.mongohq.com", 10080).db("infovis_tweets")
db.authenticate('taiar', 'taiar')

twts = db.collection('tweets')
twts.create_index("tweetid")

twitters.each do |twitter|
  puts "Fetching #{twitter}"
  url = "https://twitter.com/statuses/user_timeline/#{twitter}.json?count=#{quantidade}"
  tweets = JSON.parse(open(url).read)
  tweets.each do |tweet|
    res = twts.find({:tweetid => tweet['id']})
    if res.count == 0
      tw = {:tweetid => tweet['id'], :user => tweet['user']['name'], :date => tweet['created_at'], :text => tweet['text']}
      tw_id = twts.insert(tw)
      novos += 1
    end
  end
end

puts "#{novos} novos tweets"
