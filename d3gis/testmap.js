var xym = d3.geo.mercator();
var path = d3.geo.path().projection(xym);

var translate;
var scale;
var svg;
var map;

$(function() {
  svg = d3.select("#chart")
              .append("svg");
   
  map = svg.append("g")
           .call(d3.behavior.zoom().on("zoom", redraw));
});
 
function redraw() {
  var tx = translate[0] * d3.event.scale + d3.event.translate[0];
  var ty = translate[1] * d3.event.scale + d3.event.translate[1];
  xym.translate([tx, ty]);
  xym.scale(scale * d3.event.scale);

  /* redraw map with new scale and translation */
  map.selectAll("path").attr("d", path);
}

d3.json("roads.json", function (json) {
  /* centers in downtown belo horizonte, numbers in pixels due to scale */
  translate = [391461.74724802945, -180779.16846609005];
  scale = 3204279.5103584863;

  xym.translate(translate);
  xym.scale(scale);
   
  map.selectAll("path")
     .data(json.features) 
     .enter().append("path")
     .attr("class", "street")
     .attr("d", path);          // transform the supplied json geo path to svg
});
