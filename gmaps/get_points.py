from pyPgSQL import PgSQL
from random import random
import json

host = 'localhost'
db   = 'brunoro'
user = 'transitobh'

con = PgSQL.connect(host, db, )
cur = con.cursor()

cur.execute('SELECT rua_a, rua_b, longitude, latitude FROM cruzamentos;')

cruzamentos = []
for c in cur.fetchall():
    nome = c[0] + " com " + c[1]
    if random() < 0.01:
        cruzamentos.append({ 'name': nome, 'lat': c[2], 'lng': c[3], 'score': random() })

outjson = open('cruzamentos.json', 'w')
outjson.write(json.dumps(cruzamentos))
outjson.close()
